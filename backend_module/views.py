# -*- coding: utf-8 -*-
import inspect
import logging

from flask import request, jsonify, Blueprint


bp_backend_module = Blueprint('backend_module', __name__, url_prefix='/backend_module')


def backend_home(path):
    return 'Hello Backend [bp_backend_module.backend_home]. path = ' + path
